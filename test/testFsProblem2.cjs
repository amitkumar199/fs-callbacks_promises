const { error } = require("console");
const fs_problem2 = require("../fs-problem2.cjs");
const path= require("path");

const absolutePathOfRandomDirectory= path.join(__dirname, '../lipsum.txt');

fs_problem2.fsProblem2(absolutePathOfRandomDirectory)
    .then((data)=>{
        return fs_problem2.upper_case(data);
    })
    .then((file_name)=>{
        console.log(`files created: ${file_name}`);
        return fs_problem2.lower_case_convert(file_name);
    })
    .then((file_name)=>{
        console.log(`files created: ${file_name}`);
        return fs_problem2.sort_data(file_name);
    })
    .then((file_name)=>{
        console.log(`files created: ${file_name}`);
        return fs_problem2.delete_all_files();
    })
    .then((response_for_deletion)=>{
        console.log(response_for_deletion);
    })
    .catch((error)=>{
        console.log(error);
    });

