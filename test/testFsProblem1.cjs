const { error } = require("console");
const fs_problem1 = require("../fs-problem1.cjs");
const path= require("path");

const absolutePathOfRandomDirectory= path.join(__dirname, "random_files");
const random_files=3;

fs_problem1.fsProblem1(absolutePathOfRandomDirectory)
    .then((response)=>{
        console.log(response);
        return fs_problem1.make_random_files(absolutePathOfRandomDirectory, random_files);
    })
    .then((files_array)=>{
        for(let index=0; index<files_array.length; index++){
            console.log(`files created: ${files_array[index]}`);
        }
        console.log("all files have been created");
        return fs_problem1.remove_files(absolutePathOfRandomDirectory, files_array);
    })
    .then((final_response)=>{
        console.log(final_response);
    })
    .catch((error)=>{
        console.log(error);
    });