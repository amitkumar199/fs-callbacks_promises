const fs = require('fs');
const path = require('path');

function fsProblem1(absolutePathOfRandomDirectory){
    
    return new Promise((resolve, reject)=>{
        fs.mkdir(absolutePathOfRandomDirectory,(err)=>{
            if(err){
                reject(err.message);
            }else{
                resolve("directory created");
            }
        });
    })    
};

function make_random_files(absolutePathOfRandomDirectory, randomNumberOfFiles){

    return new Promise((resolve, reject)=>{
        let files_array = [];
        for(let index=0; index<randomNumberOfFiles; index++){
            const jsonData = {
                id: index,
                data: `Random data for file ${index}`
            };

            const json_file = `created${index}.json`;
    
            fs.writeFile(path.join(absolutePathOfRandomDirectory, json_file), JSON.stringify(jsonData), (err)=>{
                if(err){
                    reject(err.message);
                }else{
                    files_array.push(json_file);

                    if(files_array.length===randomNumberOfFiles){
                        resolve(files_array);
                    }
                }
            });
        }
    });
   
};

function remove_files(absolutePathOfRandomDirectory, files_array){
    return new Promise((resolve, reject)=>{
        for(let index=0; index<files_array.length; index++){
            const file_path= path.join(absolutePathOfRandomDirectory, files_array[index]);
    
            fs.unlink(file_path, (error)=>{
                if(error){
                    reject(error.message);
                }
           });
        }
        resolve("all files have been removed:");      
    });
    
};

module.exports = {
    fsProblem1,
    make_random_files,
    remove_files
};