const fs = require('fs');
const path = require('path');

function fsProblem2(lipsum) {
  return new Promise((resolve, reject) => {
    fs.readFile(lipsum, 'utf-8', (err, data) => {
      if (err) {
        reject(err.message);
      } else {
        resolve(data);
      }
    });
  });
};

function upper_case(data) {
  return new Promise((resolve, reject) => {
    const upper_case_content = data.toUpperCase();

    fs.writeFile('./upper_case.txt', upper_case_content, (err) => {
      if (err) {
        reject(err.message);
      } else {
        fs.appendFile('./filenames.txt', 'upper_case.txt' + '\n', (err) => {
          if (err) {
            reject(err.message);
          } else {
            resolve('upper_case.txt');
          }
        });
      }
    });
  });
};

function lower_case_convert(file_name) {
  return new Promise((resolve, reject) => {
    fs.readFile(file_name, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      }
      else {
        const lower_case_data = data.toLocaleLowerCase().split('.').join('\n');

        fs.writeFile('./lower_case.txt', lower_case_data, (err) => {
          if (err) {
            reject(err);
          }
          else {
            fs.appendFile('./filenames.txt', 'lower_case.txt' + '\n', (err) => {
              if (err) {
                reject(err);
              } else {
                resolve('lower_case.txt');
              }
            });
          }
        })
      }
    });
  });
};

function sort_data(file_name) {
  return new Promise((resolve, reject) => {
    fs.readFile(file_name, 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      }
      else {
        const sorted_data = data.split('\n').sort().join('\n');

        fs.writeFile('./sorted_data.txt', sorted_data, (err) => {
          if (err) {
            reject(err);
          }
          else {
            fs.appendFile('filenames.txt', 'sorted_data.txt' + '\n', (err) => {
              if (err) {
                reject(err);
              } else {
                resolve('sorted_data.txt');
              }
            });
          }
        });
      }
    });
  });
};

function delete_all_files() {
  return new Promise((resolve, reject) => {
    fs.readFile('./filenames.txt', 'utf-8', (err, data) => {
      if (err) {
        reject(err);
      }
      else {
        const data_array = data.split('\n');

        for (let index = 0; index < data_array.length; index++) {
          fs.unlink(data_array[index], (err) => {
            if (err) {
              reject(err.message);
            }
          });
        }
        resolve("all files from filesname.txt have been removed");
      }
    });
  });
};

module.exports = {
  fsProblem2,
  upper_case,
  lower_case_convert,
  sort_data,
  delete_all_files
};